VERSION := $(shell sed -n "s/.*VERSION.*= \{1,\}\(.*\)/\1/p;" src/handler.lua)
NAME := $(shell basename $${PWD})
UID := $(shell id -u)
GID := $(shell id -g)
SUMMARY := $(shell sed -n '/^summary: /s/^summary: //p' README.md)
export UID GID NAME VERSION

build: rockspec validate
	@find . -type f -iname "*lua~" -exec rm -f {} \;
	@docker run --rm \
          -v ${PWD}:/plugin \
	  kong /bin/sh -c "apk add --no-cache zip > /dev/null 2>&1 ; cd /plugin ; luarocks make > /dev/null 2>&1 ; luarocks pack ${NAME} 2> /dev/null ; chown ${UID}:${GID} *.rock"
	@mkdir -p dist
	@mv *.rock dist/
	@printf '\n\n Check "dist" folder \n\n'

validate:
	@if [ -z "$${VERSION}" ]; then \
	  printf "\n\nNo VERSION found in handler.lua;\nPlease set it in your object that extends the base_plugin.\nEx: plugin.VERSION = \"0.1.0-1\"\n\n"; \
	  exit 1 ;\
	else \
	  echo ${VERSION} | egrep '(\w.+)-([0-9]+)$$' > /dev/null 2>&1 ; \
	  if [ $${?} -ne 0 ]; then \
  	    printf "\n\nVERSION must follow the pattern [%%w.]+-[%%d]+\nWhich means: 0.0-0 or 0.0.0-0 or ...\nReceived: $${VERSION} \n\n"; \
	    exit 2 ; \
	  fi ; \
	fi
	@if [ -z "${SUMMARY}" ]; then \
  	  printf "\n\nNo SUMMARY found.\nPlease, create a 'README.md' file and place your summary there.\nFollow the pattern '^summary: '\nDo not use double quotes"; \
	  printf "\nExample:\nsummary: this is my summary\n\n\n" ;\
	  exit 4 ;\
	fi
	@if [ ! -f ${NAME}-${VERSION}.rockspec ]; then \
	  make rockspec; \
	fi

copy-docker-compose:
	@[ ! -f docker-compose.yaml ] && cp ../docker-compose.yaml . || printf ''

rockspec:
	@printf 'package = "%s"\nversion = "%s"\n\nsource = {\n url    = "git@bitbucket.org:leandro-carneiro/kong-sns-plugin.git",\n branch = "master"\n}\n\ndescription = {\n  summary = "%s",\n}\n\ndependencies = {\n  "lua ~> 5.1"\n}\n\nbuild = {\n  type = "builtin",\n  modules = {\n' "${NAME}" "${VERSION}" "${SUMMARY}" > ${NAME}-${VERSION}.rockspec
	@find src -type f -iname "*.lua" -exec bash -c 'printf "    [\"kong.plugins.%s.%s\"] = \"%s\",\n" "${NAME}" "$$(basename $${1/\.lua})" "{}"' _ {} \;	>> ${NAME}-${VERSION}.rockspec
	@printf "  }\n}" >> ${NAME}-${VERSION}.rockspec

clean: copy-docker-compose
	@rm -rf *.rock *.rockspec dist shm
	@find . -type f -iname "*lua~" -exec rm -f {} \;
	@docker-compose down -v

start: validate copy-docker-compose
	@docker-compose up -d

stop: copy-docker-compose
	@docker-compose down

logs: kong-logs
kong-logs:
	@docker logs -f $$(docker ps -qf name=${NAME}_kong_1) 2>&1 || true

shell: kong-bash
kong-bash:
	@docker exec -it $$(docker ps -qf name=${NAME}_kong_1) bash || true

reload: kong-reload
kong-reload:
	@docker exec -it $$(docker ps -qf name=${NAME}_kong_1) bash -c "/usr/local/bin/kong reload"

reconfigure: clean start kong-logs

config:
	@curl -s -X POST http://localhost:8001/services/ -d 'name=metadata-mock' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/metadata-mock/routes -d 'paths[]=/latest/meta-data/iam/security-credentials/my-iam-role'
	@curl -i -X POST http://localhost:8001/services/metadata-mock/plugins -F "name=request-termination"  -F 'config.status_code=200' -F 'config.content_type=application/json'  -F 'config.body={  "Code" : "Success",  "LastUpdated" : "2019-04-18T10:50:52Z",  "Type" : "AWS-HMAC",  "AccessKeyId" : "ASIA2YGQJWZG3GT6FTZ3",  "SecretAccessKey" : "wugX+VU3AogcjsEbsgh9Crw85a9hz4vJMsbE8jZz",  "Token" : "AgoJb3JpZ2luX2VjEGsaCXNhLWVhc3QtMSJHMEUCIE8Pf+tU7iEZsRq1Vt3LhjpcaWhBLjQPZeeSvw4Lv/2KAiEArE2+2NHkRG2pi9pBrt1xF5cS1u8CaAjm7t0JlWPWC0Yq2gMIRBABGgw3MzkxNzEyMTkwMjEiDBDR8yOXlu/DncRUqiq3A4JCKXgxyxHuhYqw9Fc+wZ3nUUxyypWOBSDhnAXPU98HBnHX0J0c8jL4ay/KaXg7yeGUBrnpwRLIYPPcdJ8EE+2a/PrxdGSnGJjtFIWhcBEz/JEiRvV/01IpiGReaN0rT1t36TEIoYI2M7eHkqP3q4BvQeSyJha2TyqM+D4PihwXJjjLflC0PJwoILgcTbE3JpVToaniM8oTq0WT7Mw3/Vops99hoaiCTETrCk7A/xKPp4Ndrb6NVzDilEbO0cyKr5hqBH+QGuBknimk6rey2l4MmCmB4+NsS2c2a/egbCV92zzg9zN5PnIPauNdomM44rTxOL84AAlSA1GH22Q6HrFzRCfHQtL5UQaX6FgDNTc9IbXyWwpHf8bfeaxrlaVMZmBJxZp0CfTSaomz4zLYQCixJuG2HowJ/DWiw0gLXEaLAC6R+SgsRQASGi1TwGD70JUdL9ALqoner0PClThIfp/E9PvL7TwiyV+GF8Vd2cnlOrwaWwHvQ2tuIEGH7VKm943UR6G45KMncSvCCdTd6GL0+MGra2xPUi+6o9xY0GJq4mbp3aUNJae1JVO/6tpulhA1Mes090Awxa3h5QU6tAEQsthFypXWOaq0xbfRWlP6m6IxPjatlhiwu0lbXCKf6lKPFnkr7gJUm6Zq3+Jojv2y9bOcSVHqZ34vRnqTN4dfQAPRXp8rsnHN6TIQ9lUJ871yIQ72CBL8VGM5n/3jRoAikNCdX2rFkW85FVoaoarHgiD/JeP8T+Hw6zxe9AukieS/0CauB1XCOmnw1HkdE+fCX5qZ0HdEcWNs1e6HsiUA9FMVFFQshoNAo34ra/nkNEzFZmo=",  "Expiration" : "2019-04-18T16:55:07Z"}'
	@curl -s -X POST http://localhost:8001/services/ -d 'name=sns' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/sns/routes -d 'paths[]=/sns'
	@curl -s -X POST http://localhost:8001/services/sns/plugins --data "name=${NAME}" --data-urlencode "config.aws_ami_role=my-iam-role" --data "config.aws_region=sa-east-1" --data "config.topic_name=teste_caio" --data "config.aws_metadata_url=http://localhost:8000/latest/meta-data/iam/security-credentials/" --data "config.aws_account_id=739171219021"


config-aux:
	@[ ! -f aux.lua ] && echo -e 'ngx.say("hello from aux - edit aux.lua and run make patch-aux")\nngx.exit(200)' > aux.lua || printf ''
	@curl -s -X POST http://localhost:8001/services/ -d 'name=aux' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/aux/routes -d 'paths[]=/aux'
	@curl -i -X POST http://localhost:8001/services/aux/plugins -F "name=pre-function" -F "config.functions=@aux.lua"

patch-aux:
	@curl -i -X PATCH http://localhost:8001/plugins/$$(curl -s http://localhost:8001/plugins/ | jq -r ".data[] |  select (.name|test(\"pre-function\")) .id")      -F "name=pre-function"      -F "config.functions=@aux.lua"
	@echo " "

req-aux:
	@curl -s http://localhost:8000/aux

